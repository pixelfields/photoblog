#!/usr/bin/env python
import os, sys, pprint, shutil
from fabric.api import local, run, task, hosts, sudo, settings, prompt

@task
def init(initcms='n'):
    """
    Inicializuje vyvojove prostredi. Pro spravnou funkci musi byt aktivovan virtualenv. arguments: initcms=n|y default n
    """
    app_name = raw_input('Vlozte jmeno aplikace: ')
    if app_name == '':
        sys.exit('jmeno aplikace nesmi byt prazdny retezec')
    replace_template_name(app_name)
    init_logfile(app_name)
    init_local(app_name)
    local('python manage.py syncdb')
    local('python manage.py test sandbox')
    if initcms == 'y':
        init_cms(app_name)
        local('python manage.py syncdb')
    
    
@task
def rmgit():
    """
    Odstrani .git adresar. Doporuceno je spusteni az po init() tasku.
    """
    confirm = prompt('Opravdu smazat git adresar? y|n', default='n')
    if confirm == 'y':
        shutil.rmtree('../.git')
        
    
def init_local(app_name):
    init_example(app_name, 'local.py.example')
    

def init_cms(app_name):
    init_example(app_name, 'cms.py.example')
   
    
def init_example(app_name, filename=None):
    try:
        os.rename(os.path.join(app_name, 'settings', filename), os.path.join(app_name, 'settings', '.'.join(filename.split('.')[:-1])))
    except OSError:
        print """
        Soubor %s nebyl nalezen. Preskakuji vytvoreni local.py
        """ % str(filename)
    
    
def rename(filename):
    try:
        os.rename(os.path.join(app_name, 'settings', 'local.py.example'), os.path.join(app_name, 'settings', 'local.py'))
    except OSError:
        print """
        Soubor local.py.example nebyl nalezen. Preskakuji vytvoreni local.py
        """
    
    
def init_logfile(app_name):
    basic_path = os.path.join('/var', 'log', 'django-apps')
    app_path = '%s.log' % os.path.join(basic_path, app_name)
    commands = []
    if not os.path.exists(basic_path):
        commands.append('mkdir %s' % basic_path)
    commands.append('touch %s' % app_path)
    commands.append('chmod 666 %s' % app_path)
    
    local('su -c "%s"' % ' && '.join(commands))
    

def replace_template_name(app_name):
    tree = get_app_tree()
    for filename in tree:
        with open(filename, 'r+') as f:
            _con = f.read()
            f.seek(0)
            f.truncate()
            f.write(_con.replace('<app>', app_name))
    
    try:
        os.rename(os.path.join('<app>'), os.path.join(app_name))
    except OSError:
        print """
        Pravdepodobne byl init() jiz spusten nebo spoustite ulohu ze spatne cesty.
        fab init musi byt spousteno z adresare ve kterem se nachazi fabfile.py
        
        Preskakuji tento krok!
        """
    
    
def get_app_tree(root='.', ignore=('fabfile.py', '.pyc')):
    app_dir = None
    tree = []
    for dirname, dirnames, filenames in os.walk(root):
        for filename in filenames:
            if not filename.endswith(ignore):
                tree.append(os.path.join(dirname, filename))
    return tree

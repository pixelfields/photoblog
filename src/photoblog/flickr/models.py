#!/usr/bin/env python
# encoding: utf-8

from django.db import models
from django.conf import settings

from taggit.managers import TaggableManager
from sorl.thumbnail import get_thumbnail

from .utils import FLICKR_PHOTO_SIZES

URL_BASE = getattr(settings, 'FLICKR_URL_BASE', 'http://www.flickr.com/')

user_model = getattr(settings, 'AUTH_USER_MODEL')


class FlickrUser(models.Model):

    user = models.OneToOneField(user_model)
    flickr_id = models.CharField(max_length=50, null=True, blank=True)
    nsid = models.CharField(max_length=32, null=True, blank=True)
    username = models.CharField(max_length=64, null=True, blank=True)
    realname = models.CharField(max_length=64, null=True, blank=True)
    photosurl = models.URLField(max_length=255, null=True, blank=True)
    profileurl = models.URLField(max_length=255, null=True, blank=True)
    mobileurl = models.URLField(max_length=255, null=True, blank=True)
    iconserver = models.CharField(max_length=4, null=True, blank=True)
    iconfarm = models.PositiveSmallIntegerField(null=True, blank=True)
    path_alias = models.CharField(max_length=32, null=True, blank=True)
    ispro = models.NullBooleanField()
    tzoffset = models.CharField(max_length=6, null=True, blank=True)
    location = models.CharField(max_length=200, null=True, blank=True)

    token = models.CharField(max_length=128, null=True, blank=True)
    perms = models.CharField(max_length=32, null=True, blank=True)
    last_sync = models.DateTimeField(null=True, blank=True)

    class Meta:
        ordering = ['id']

    def __unicode__(self):
        return u"%s" % self.username

    @property
    def flickr_page_url(self):
        if self.username:
            return '%sphotos/%s/' % (URL_BASE, self.username)
        return '%sphotos/%s/' % (URL_BASE, self.nsid)


class FlickrPhotoExif(models.Model):

    exif = models.TextField(null=True, blank=True)
    exif_camera = models.CharField(max_length=50, null=True, blank=True)
    exif_exposure = models.CharField(max_length=50, null=True, blank=True)
    exif_aperture = models.CharField(max_length=50, null=True, blank=True)
    exif_iso = models.IntegerField(null=True, blank=True)
    exif_focal = models.CharField(max_length=50, null=True, blank=True)
    exif_flash = models.CharField(max_length=50, null=True, blank=True)
    exif_date_taken = models.DateTimeField(null=True, blank=True)

    def __unicode__(self):
        return u'%s' % self.exif_camera


class FlickrPhotoGeo(models.Model):

    geo_latitude = models.FloatField(null=True, blank=True)
    geo_longitude = models.FloatField(null=True, blank=True)
    geo_accuracy = models.PositiveSmallIntegerField(null=True, blank=True)

    def __unicode__(self):

        return u'%s, %s' % (str(self.geo_latitude), str(self.geo_longitude))


class ModelBase(models.Model):

    publish = models.BooleanField(default=True)
    last_sync = models.DateTimeField(null=True, blank=True, auto_now=True)

    class Meta:
        abstract = True


class FlickrPhoto(ModelBase):

    user = models.ForeignKey(FlickrUser)
    flickr_id = models.BigIntegerField()
    secret = models.CharField(max_length=10)
    server = models.PositiveSmallIntegerField()
    title = models.CharField(max_length=255, null=True, blank=True)

    ispublic = models.NullBooleanField()
    isfriend = models.NullBooleanField()
    isfamily = models.NullBooleanField()

    exif = models.OneToOneField(FlickrPhotoExif, null=True, blank=True)
    geo = models.OneToOneField(FlickrPhotoGeo, null=True, blank=True)
    tags = TaggableManager(blank=True)

    publish_on_hp = models.BooleanField(default=False)
    slug = models.SlugField(null=True, blank=True)

    class Meta:
        ordering = ['-exif__exif_date_taken']
        get_latest_by = 'exif__exif_date_taken'

    def __unicode__(self):
        return u'%s' % self.title

    @property
    def square_75_url(self):
        from .models import FlickrPhotoSize
        try:
            url = FlickrPhotoSize.objects.get(size='square', photo=self)
        except FlickrPhotoSize.DoesNotExist:
            return None
        return url.source

    @property
    def square_150_url(self):
        from .models import FlickrPhotoSize
        try:
            url = FlickrPhotoSize.objects.get(size='largesquare', photo=self)
        except FlickrPhotoSize.DoesNotExist:
            return None
        return url.source

    @property
    def original_url(self):
        from .models import FlickrPhotoSize
        try:
            url = FlickrPhotoSize.objects.get(size='ori', photo=self)
        except FlickrPhotoSize.DoesNotExist:
            return None
        return url.source

    @property
    def thumbnail_url(self):
        from .models import FlickrPhotoSize
        try:
            url = FlickrPhotoSize.objects.get(size='thumb', photo=self)
        except FlickrPhotoSize.DoesNotExist:
            return None
        return url.source

    @property
    def small_320_url(self):
        from .models import FlickrPhotoSize
        try:
            url = FlickrPhotoSize.objects.get(size='small320', photo=self)
        except FlickrPhotoSize.DoesNotExist:
            return None
        return url.source

    @property
    def medium_url(self):
        from .models import FlickrPhotoSize
        try:
            url = FlickrPhotoSize.objects.get(size='medium', photo=self)
        except FlickrPhotoSize.DoesNotExist:
            return None
        return url.source

    @property
    def large_url(self):
        from .models import FlickrPhotoSize
        try:
            url = FlickrPhotoSize.objects.get(size='large', photo=self)
        except FlickrPhotoSize.DoesNotExist:
            return None
        return url.source

    def flick_thumbnail_square_75(self):
        im = get_thumbnail(self.square_75_url, '75x75', crop='center', quality=99)
        return '<img src="%s"/>' % im.url
    flick_thumbnail_square_75.allow_tags = True

    @staticmethod
    def get_latest_by_exif_taken():
        latest = FlickrPhoto.objects.exclude(exif__exif_date_taken__isnull=True).order_by('-exif__exif_date_taken')
        return latest

    @models.permalink
    def get_absolute_url(self):
        return 'photo', (), {'slug': self.slug}


class FlickrPhotoSize(models.Model):

    photo = models.ForeignKey(FlickrPhoto, related_name='sizes')
    size = models.CharField(max_length=11, choices=[(v['label'], k) for k, v in FLICKR_PHOTO_SIZES.iteritems()])
    width = models.PositiveIntegerField(null=True, blank=True)
    height = models.PositiveIntegerField(null=True, blank=True)
    source = models.URLField(null=True, blank=True)
    url = models.URLField(null=True, blank=True)
    media = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        unique_together = (('photo', 'size'),)


class FlickrPhotoSet(ModelBase):

    flickr_id = models.CharField(max_length=50, null=True, blank=True)
    server = models.PositiveSmallIntegerField()
    farm = models.PositiveSmallIntegerField()
    secret = models.CharField(max_length=10)
    title = models.CharField(max_length=200)
    description = models.TextField(null=True, blank=True)
    primary = models.ForeignKey(FlickrPhoto, null=True, blank=True, related_name='primary_photo')
    date_posted = models.DateTimeField(null=True, blank=True)
    date_updated = models.DateTimeField(null=True, blank=True)
    photos = models.ManyToManyField(FlickrPhoto, null=True, blank=True, related_name='set_photos')
    slug = models.SlugField(null=True, blank=True)

    class Meta:
        ordering = ('-date_posted', '-id',)
        get_latest_by = 'date_posted'

    def __unicode__(self):
        return u'%s' % self.title

    @models.permalink
    def get_absolute_url(self):
        return 'set', (), {'slug': self.slug}

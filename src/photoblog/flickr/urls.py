from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^auth/$', "photoblog.flickr.views.auth", name="flickr_auth"),
    url(r'^auth/complete/$', "photoblog.flickr.views.callback", name="flickr_callback"),

)

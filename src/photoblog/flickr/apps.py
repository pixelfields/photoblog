from django.apps import AppConfig

class FlickrConfig(AppConfig):

    name = 'photoblog_flickr'
    verbose_name = "Flickr"
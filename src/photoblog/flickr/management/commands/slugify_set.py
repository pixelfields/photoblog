from django.core.management.base import BaseCommand
from django.template.defaultfilters import slugify

from photoblog.flickr.models import FlickrPhotoSet


class Command(BaseCommand):
    help = "Create slugs for sets."

    def handle(self, *args, **options):

        objects = FlickrPhotoSet.objects.filter(slug=None)

        for obj in objects:
            try:
                obj.slug = slugify(obj.title)
            except Exception as e:
                self.stderr.write(str(e))

            if obj.slug is not None:
                obj.save()

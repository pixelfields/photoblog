# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth import get_user_model
from django.conf import settings
from django.utils import timezone
from django.template.defaultfilters import slugify

from optparse import make_option
import time
import flickrapi
import datetime
import requests
from pprint import pprint
import sys

from photoblog.flickr.models import *


class Command(BaseCommand):
    help = "Sync photos from flickr."

    option_list = BaseCommand.option_list
    option_list = option_list + (make_option('--user', '-u',
                                             action='store',
                                             dest='user_id',
                                             default=1,
                                             help='Sync for a particular user. Default is 1 (in most cases '
                                                  'it\'s the admin and you\'re using it only '
                                                  'for yourself).'),
                                 make_option('--all', '-a',
                                             action='store_true',
                                             dest='all',
                                             default=False,
                                             help='By default downloads only photos which have not been downloaded '
                                                  '(default behavior). Use this option to (re)download all.'),
                                 make_option('--sets', '-s',
                                             action='store_true',
                                             dest='sets',
                                             default=False,
                                             help='Sync photosets.'),
                                 make_option('--exif', '-e',
                                             action='store_true',
                                             dest='exif',
                                             default=False,
                                             help='Sync exif info.'),
                                 make_option('--geo', '-g',
                                             action='store_true',
                                             dest='geo',
                                             default=False,
                                             help='Sync geo data.'),
                                 make_option('--tags', '-t',
                                             action='store_true',
                                             dest='tags',
                                             default=False,
                                             help='Sync tags.'),
                                 make_option('--sizes', '-z',
                                             action='store_true',
                                             dest='sizes',
                                             default=False,
                                             help='Sync sizes.'),
                                 )

    def __init__(self):
        super(Command, self).__init__()
        self.token = None
        self.nsid = None
        self.api = None
        self.fuser = None

        try:
            self.api_key = getattr(settings, 'FLICKR_KEY')
        except Exception as e:
            raise CommandError(str(e))

        try:
            self.api_secret = getattr(settings, 'FLICKR_SECRET')
        except Exception as e:
            raise CommandError(str(e))

    def handle(self, *args, **options):

        t1 = time.time()

        self.stdout.write('Syncing photos from Flickr to local db.')

        # get flickr user saved in local db => needed for api key
        self.get_flickr_user(**options)
        self.authenticate(**options)

        # always sync user info
        self.user_info(**options)

        if options.get('all'):
            # sync all user photos
            self.sync_photos(**options)

        if options.get('sets'):
            # sync photosets
            self.sync_sets(**options)

        if options.get('exif'):
            # sync exif data
            self.sync_exif(**options)

        if options.get('geo'):
            # sync geo data
            self.sync_geo(**options)

        if options.get('tags'):
            # sync tags
            self.sync_tags(**options)

        if options.get('sizes'):
            # sync sizes
            self.sync_sizes(**options)

        t2 = time.time()
        self.stdout.write('Exec time: %s' % str(round(t2 - t1)))

    def get_flickr_user(self, **options):
        user_id = options.get('user_id')
        self.stdout.write('Syncing data for user ID: %s' % user_id)

        try:
            cuser = get_user_model().objects.get(pk=user_id)
        except get_user_model().DoesNotExist:
            raise CommandError('User with ID: %s does not exist.' % user_id)

        try:
            self.fuser = FlickrUser.objects.get(user=cuser)
        except FlickrUser.DoesNotExist:
            raise CommandError('Flickr user associated with given user ID: %s does not exist.' % cuser.pk)

        self.token = self.fuser.token
        self.nsid = self.fuser.nsid

    def authenticate(self, **options):
        self.api = flickrapi.FlickrAPI(self.api_key,
                                       self.api_secret, token=self.token,
                                       store_token=False, format='etree')

    def user_info(self, **options):

        try:
            resp = self.api.people_getInfo(user_id=self.nsid)
        except flickrapi.FlickrError as e:
            raise CommandError(str(e))

        for node in resp.iter('person'):
            ispro = node.attrib.get('ispro')
            path_alias = node.attrib.get('path_alias')
            iconserver = node.attrib.get('iconserver')
            flickr_id = node.attrib.get('id')
            iconfarm = node.attrib.get('iconfarm')

        self.fuser.username = resp._children[0]._children[0].text
        self.fuser.realname = resp._children[0]._children[1].text

        self.fuser.tzoffset = resp._children[0]._children[4].attrib['offset']
        self.fuser.photosurl = resp._children[0]._children[6].text
        self.fuser.profileurl = resp._children[0]._children[7].text
        self.fuser.mobileurl = resp._children[0]._children[8].text

        self.fuser.ispro = ispro
        self.fuser.path_alias = path_alias
        self.fuser.iconserver = iconserver
        self.fuser.flickr_id = flickr_id
        self.fuser.iconfarm = iconfarm
        self.fuser.last_sync = timezone.now()
        self.fuser.save()

    def sync_photos(self, **options):
        self.stdout.write('Syncing photos for flickr user: %s' % self.fuser)

        # calling api for all photos
        kwargs = {
            'user_id': 'me',
        }

        # first run
        resp = self.api.people_getPhotos(**kwargs)
        if resp.attrib['stat'] == 'ok':
            # if response is ok
            photos_node = resp.find('photos')
            curr_page = photos_node.attrib['page']
            count_pages = photos_node.attrib['pages']
            total_photos = photos_node.attrib['total']
            self.stdout.write('Total photos is %s' % total_photos)
            self.stdout.write('Current page is %s' % curr_page)
            self.stdout.write('Count pages is %s' % count_pages)

            # process first page
            self.stdout.write('Processing page no: %s ...' % curr_page)
            self._process_photos_node(resp.findall('photos/photo'))

            if curr_page != count_pages:
                # process all other pages
                for page in range(int(curr_page)+1, int(count_pages)+1):
                    self.stdout.write('Processing page no: %s ...' % page)
                    kwargs.update({'page': str(page)})
                    resp = self.api.people_getPhotos(**kwargs)
                    self._process_photos_node(resp.findall('photos/photo'))

    def sync_sets(self, **options):
        self.stdout.write('Syncing photo sets for flickr user: %s' % self.fuser)

        # calling api for all photos
        kwargs = {}

        # first run
        resp = self.api.photosets_getList()
        if resp.attrib['stat'] == 'ok':
            sets_node = resp.find('photosets')
            curr_page = sets_node.attrib['page']
            count_pages = sets_node.attrib['pages']
            total_sets = sets_node.attrib['total']
            self.stdout.write('Total sets is %s' % total_sets)
            self.stdout.write('Current page is %s' % curr_page)
            self.stdout.write('Count pages is %s' % count_pages)
            # process first page
            self.stdout.write('Processing page no: %s ...' % curr_page)
            self._process_sets_node(resp.findall('photosets/photoset'))

            if curr_page != count_pages:
                # process all other pages
                for page in range(int(curr_page)+1, int(count_pages)+1):
                    self.stdout.write('Processing page no: %s ...' % page)
                    kwargs.update({'page': str(page)})
                    resp = self.api.photosets_getList(**kwargs)
                    self._process_sets_node(resp.findall('photosets/photoset'))

    def sync_sizes(self, **options):
        self.stdout.write('Syncing tags for all photos in database which do not have this info synced.')
        queryset = FlickrPhoto.objects.all()
        for photo in queryset:
            # find sizes
            s_queryset = FlickrPhotoSize.objects.filter(photo=photo)
            if len(s_queryset) == 0:
                resp = self.api.photos_getSizes(photo_id=photo.flickr_id)
                if resp.attrib['stat'] == 'ok':
                    s_nodes = resp.findall('sizes/size')
                    for node in s_nodes:
                        obj = FlickrPhotoSize()
                        obj.photo = photo
                        obj.size = FLICKR_PHOTO_SIZES[node.attrib.get('label')]['label']
                        obj.width = node.attrib.get('width')
                        obj.height = node.attrib.get('height')
                        obj.source = node.attrib.get('source')
                        obj.url = node.attrib.get('url')
                        obj.media = node.attrib.get('media')
                        obj.save()

    def sync_tags(self, **options):
        self.stdout.write('Syncing tags for all photos in database.')
        queryset = FlickrPhoto.objects.all()
        for photo in queryset:
            tags = []
            resp = self.api.tags_getListPhoto(photo_id=photo.flickr_id)
            if resp.attrib['stat'] == 'ok':
                tag_nodes = resp.findall('photo/tags/tag')
                for tag in tag_nodes:
                    tags.append(tag.attrib['raw'])
            if len(tags) == 0:
                photo.tags.clear()
            else:
                photo.tags.set(*tags)

    def sync_geo(self, **options):
        self.stdout.write('Syncing geo data for all photos in database which do not have geo info.')
        queryset = FlickrPhoto.objects.filter(geo=None)
        self.stdout.write('Processing %s photos.' % queryset.count())
        for photo in queryset:
            try:
                resp = self.api.photos_geo_getLocation(photo_id=photo.flickr_id)
            except flickrapi.FlickrError as e:
                self.stderr.write('%s - photo ID: %s' % (str(e), photo.id))
                continue
            if resp.attrib['stat'] == 'ok':
                node = resp.find('photo/location')
                g = FlickrPhotoGeo()
                g.geo_latitude = node.attrib['latitude']
                g.geo_longitude = node.attrib['longitude']
                g.geo_accuracy = node.attrib['accuracy']
                g.save()
                photo.geo = g
                photo.save()

    def sync_exif(self, async=True, **options):
        from xml.etree.ElementTree import tostring
        self.stdout.write('Syncing exif data for all photos in database which do not have exif info.')
        if async is True:
            queryset = FlickrPhoto.objects.filter(exif=None)
            self.stdout.write('Processing %s photos.' % queryset.count())
            for photo in queryset:
                resp = self.api.photos_getExif(photo_id=photo.flickr_id)
                if resp.attrib['stat'] == 'ok':
                    node = resp.find('photo')
                    nodes = resp.findall('photo/exif')

                    e = FlickrPhotoExif()
                    e.exif = tostring(node, 'utf-8', method="xml")
                    e.exif_camera = node.attrib['camera']

                    for n in nodes:
                        if n.attrib['label'] == 'Exposure':
                            if n.find('clean') is not None:
                                e.exif_exposure = n.find('clean').text
                            else:
                                e.exif_exposure = n.find('raw').text
                        if n.attrib['label'] == 'Aperture':
                            if n.find('clean') is not None:
                                e.exif_aperture = n.find('clean').text
                            else:
                                e.exif_aperture = n.find('raw').text
                        if n.attrib['label'] == 'ISOSetting':
                            if n.find('clean') is not None:
                                e.exif_iso = n.find('clean').text
                            else:
                                e.exif_iso = n.find('raw').text
                        if n.attrib['label'] == 'Focal Length':
                            if n.find('clean') is not None:
                                e.exif_focal = n.find('clean').text
                            else:
                                e.exif_focal = n.find('raw').text
                        if n.attrib['label'] == 'Flash':
                            if n.find('clean') is not None:
                                e.exif_flash = n.find('clean').text
                            else:
                                e.exif_flash = n.find('raw').text
                        if n.attrib['label'] == 'Date and Time (Digitized)':
                            d, h = n.find('raw').text.split(' ')
                            d = d.replace(':', '-')
                            e.exif_date_taken = '%s %s' % (d, h)
                    e.save()
                    photo.exif = e
                    photo.save()

    def _process_sets_node(self, node):
        for n in node:
            self.stdout.write('Processing set with flickr id: %s ...' % n.attrib['id'])
            try:
                set = FlickrPhotoSet.objects.get(flickr_id=n.attrib['id'])
                self.stdout.write('Set with given id found in databases, updating info ...')
            except FlickrPhotoSet.DoesNotExist:
                set = FlickrPhotoSet()
                self.stdout.write('Set with given id not found in databases, storing info ...')

            set.flickr_id = n.attrib['id']
            set.primary = None
            set.secret = n.attrib['secret']
            set.server = n.attrib['server']
            set.farm = n.attrib['farm']
            set.title = n.find('title').text
            set.description = n.find('description').text
            set.date_posted = self._convert_to_aware(int(n.attrib['date_create']))
            set.date_updated = self._convert_to_aware(int(n.attrib['date_update']))
            set.slug = slugify(n.find('title').text)
            set.save()

            # pair mm to photos
            self._mm_sets(set)

    def _mm_sets(self, photoset):
        self.stdout.write('Pairing photoswith  set with flickr id: %s ...' % photoset.flickr_id)
        kwargs = {'photoset_id': photoset.flickr_id}

        # first run
        resp = self.api.photosets_getPhotos(**kwargs)
        if resp.attrib['stat'] == 'ok':
            set_node = resp.find('photoset')
            curr_page = set_node.attrib['page']
            count_pages = set_node.attrib['pages']
            total_photos = set_node.attrib['total']
            self.stdout.write('Total photos in set is %s' % total_photos)
            self.stdout.write('Current page is %s' % curr_page)
            self.stdout.write('Count pages is %s' % count_pages)
            # process first page
            self.stdout.write('Processing page no: %s ...' % curr_page)
            self._process_pair_mm_sets(resp.findall('photoset/photo'), photoset)

            if curr_page != count_pages:
                # process all other pages
                for page in range(int(curr_page)+1, int(count_pages)+1):
                    self.stdout.write('Processing page no: %s ...' % page)
                    kwargs.update({'page': str(page)})
                    resp = self.api.photosets_getPhotos(**kwargs)
                    self._process_sets_node(resp.findall('photoset/photo'))

    def _process_pair_mm_sets(self, node, photoset):
        for n in node:
            self.stdout.write('Processing photo with flickr id: %s ...' % n.attrib['id'])
            try:
                photo = FlickrPhoto.objects.get(flickr_id=n.attrib['id'])
                self.stdout.write('Found photo with id: %s ...' % photo.id)
            except FlickrPhoto.DoesNotExist:
                self.stdout.write('Photo with flickr id: %s not found, continue loop ...' % n.attrib['id'])
                continue
            photoset.photos.add(photo)
            if n.attrib['isprimary'] == '1':
                photoset.primary = photo
                photoset.save()

    def _convert_to_aware(self, s):
        import pytz

        utc_tz = pytz.timezone('UTC')
        utc_dt = utc_tz.localize(datetime.datetime.utcfromtimestamp(s))

        local_tz = pytz.timezone(getattr(settings, 'TIME_ZONE'))
        local_dt = local_tz.normalize(utc_dt.astimezone(local_tz))

        return local_dt

    def _process_photos_node(self, node):
        for n in node:
            self.stdout.write('Processing photo with flickr id: %s ...' % n.attrib['id'])
            try:
                photo = FlickrPhoto.objects.get(flickr_id=n.attrib['id'])
                self.stdout.write('Photo with given id found in databases, updating info ...')
            except FlickrPhoto.DoesNotExist:
                photo = FlickrPhoto()
                self.stdout.write('Photo with given id not found in databases, storing info ...')

            photo.user = self.fuser
            photo.flickr_id = n.attrib['id']
            photo.secret = n.attrib['secret']
            photo.server = n.attrib['server']
            photo.title = n.attrib['title']
            photo.ispublic = n.attrib['ispublic']
            photo.isfriend = n.attrib['isfriend']
            photo.isfamily = n.attrib['isfamily']
            str_to_slug = "%s-%s" % (n.attrib['id'], n.attrib['title'])
            photo.slug = slugify(str_to_slug[:50])
            photo.save()




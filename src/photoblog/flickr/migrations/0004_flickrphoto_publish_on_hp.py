# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('flickr', '0003_flickrphotoexif_exif_date_taken'),
    ]

    operations = [
        migrations.AddField(
            model_name='flickrphoto',
            name='publish_on_hp',
            field=models.BooleanField(default=False),
        ),
    ]

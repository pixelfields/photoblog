# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('taggit', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FlickrPhoto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('publish', models.BooleanField(default=True)),
                ('last_sync', models.DateTimeField(auto_now=True, null=True)),
                ('flickr_id', models.BigIntegerField()),
                ('secret', models.CharField(max_length=10)),
                ('server', models.PositiveSmallIntegerField()),
                ('title', models.CharField(max_length=255, null=True, blank=True)),
                ('ispublic', models.NullBooleanField()),
                ('isfriend', models.NullBooleanField()),
                ('isfamily', models.NullBooleanField()),
            ],
            options={
                'ordering': ['id'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FlickrPhotoExif',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('exif', models.TextField(null=True, blank=True)),
                ('exif_camera', models.CharField(max_length=50, null=True, blank=True)),
                ('exif_exposure', models.CharField(max_length=50, null=True, blank=True)),
                ('exif_aperture', models.CharField(max_length=50, null=True, blank=True)),
                ('exif_iso', models.IntegerField(null=True, blank=True)),
                ('exif_focal', models.CharField(max_length=50, null=True, blank=True)),
                ('exif_flash', models.CharField(max_length=50, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FlickrPhotoGeo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('geo_latitude', models.FloatField(null=True, blank=True)),
                ('geo_longitude', models.FloatField(null=True, blank=True)),
                ('geo_accuracy', models.PositiveSmallIntegerField(null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FlickrPhotoSet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('publish', models.BooleanField(default=True)),
                ('last_sync', models.DateTimeField(auto_now=True, null=True)),
                ('flickr_id', models.CharField(max_length=50, null=True, blank=True)),
                ('server', models.PositiveSmallIntegerField()),
                ('farm', models.PositiveSmallIntegerField()),
                ('secret', models.CharField(max_length=10)),
                ('title', models.CharField(max_length=200)),
                ('description', models.TextField(null=True, blank=True)),
                ('date_posted', models.DateTimeField(null=True, blank=True)),
                ('date_updated', models.DateTimeField(null=True, blank=True)),
                ('photos', models.ManyToManyField(related_name=b'set_photos', null=True, to='flickr.FlickrPhoto', blank=True)),
                ('primary', models.ForeignKey(related_name=b'primary_photo', blank=True, to='flickr.FlickrPhoto', null=True)),
            ],
            options={
                'ordering': ('-date_posted', '-id'),
                'get_latest_by': 'date_posted',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FlickrPhotoSize',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('size', models.CharField(max_length=11, choices=[(b'large1600', b'Large 1600'), (b'large', b'Large'), (b'medium', b'Medium'), (b'medium640', b'Medium 640'), (b'largesquare', b'Large Square'), (b'medium800', b'Medium 800'), (b'small', b'Small'), (b'thumb', b'Thumbnail'), (b'square', b'Square'), (b'small320', b'Small 320'), (b'ori', b'Original'), (b'large2048', b'Large 2048')])),
                ('width', models.PositiveIntegerField(null=True, blank=True)),
                ('height', models.PositiveIntegerField(null=True, blank=True)),
                ('source', models.URLField(null=True, blank=True)),
                ('url', models.URLField(null=True, blank=True)),
                ('media', models.CharField(max_length=20, null=True, blank=True)),
                ('photo', models.ForeignKey(related_name=b'sizes', to='flickr.FlickrPhoto')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FlickrUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('flickr_id', models.CharField(max_length=50, null=True, blank=True)),
                ('nsid', models.CharField(max_length=32, null=True, blank=True)),
                ('username', models.CharField(max_length=64, null=True, blank=True)),
                ('realname', models.CharField(max_length=64, null=True, blank=True)),
                ('photosurl', models.URLField(max_length=255, null=True, blank=True)),
                ('profileurl', models.URLField(max_length=255, null=True, blank=True)),
                ('mobileurl', models.URLField(max_length=255, null=True, blank=True)),
                ('iconserver', models.CharField(max_length=4, null=True, blank=True)),
                ('iconfarm', models.PositiveSmallIntegerField(null=True, blank=True)),
                ('path_alias', models.CharField(max_length=32, null=True, blank=True)),
                ('ispro', models.NullBooleanField()),
                ('tzoffset', models.CharField(max_length=6, null=True, blank=True)),
                ('location', models.CharField(max_length=200, null=True, blank=True)),
                ('token', models.CharField(max_length=128, null=True, blank=True)),
                ('perms', models.CharField(max_length=32, null=True, blank=True)),
                ('last_sync', models.DateTimeField(null=True, blank=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['id'],
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='flickrphotosize',
            unique_together=set([('photo', 'size')]),
        ),
        migrations.AddField(
            model_name='flickrphoto',
            name='exif',
            field=models.OneToOneField(null=True, blank=True, to='flickr.FlickrPhotoExif'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='flickrphoto',
            name='geo',
            field=models.OneToOneField(null=True, blank=True, to='flickr.FlickrPhotoGeo'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='flickrphoto',
            name='tags',
            field=taggit.managers.TaggableManager(to='taggit.Tag', through='taggit.TaggedItem', blank=True, help_text='A comma-separated list of tags.', verbose_name='Tags'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='flickrphoto',
            name='user',
            field=models.ForeignKey(to='flickr.FlickrUser'),
            preserve_default=True,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('flickr', '0004_flickrphoto_publish_on_hp'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='flickrphoto',
            options={'ordering': ['-exif__exif_date_taken'], 'get_latest_by': 'exif__exif_date_taken'},
        ),
        migrations.AddField(
            model_name='flickrphoto',
            name='slug',
            field=models.SlugField(null=True, blank=True),
        ),
    ]

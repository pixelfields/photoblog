# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('flickr', '0002_flickrphotoset_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='flickrphotoexif',
            name='exif_date_taken',
            field=models.DateTimeField(null=True, blank=True),
            preserve_default=True,
        ),
    ]

from django.http import HttpResponse, HttpResponseRedirect
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse

import flickrapi
import logging
from pprint import pprint

from photoblog.flickr.decorators import require_flickr_auth
from photoblog.flickr.models import FlickrUser

log = logging.getLogger('event')


@login_required
@require_flickr_auth
def auth(request):

    if 'token' in request.session:
        f = flickrapi.FlickrAPI(settings.FLICKR_KEY,
                                settings.FLICKR_SECRET, token=request.session['token'],
                                store_token=False, format='etree')
        try:
            resp = f.auth_checkToken()
        except flickrapi.FlickrError as e:
            log.error(str(e))
            return HttpResponse('Authenticate! But we obtain error from flickr api while fetching token info: %s'
                                % str(e))

        for node in resp.iter('user'):
            username = node.attrib.get('username')
            fullname = node.attrib.get('fullname')
            nsid = node.attrib.get('nsid')

        for node in resp.iter():
            if node.tag == 'perms':
                perms = node.text

        FlickrUser.objects.get_or_create(
            user=request.user,
            token=request.session['token'],
            nsid=nsid,
            username=username,
            realname=fullname,
            perms=perms
        )

    return HttpResponse('Authenticate! Token = %s' % request.session['token'])


def callback(request):
    log.info('We got a callback from Flickr, store the token to session')

    f = flickrapi.FlickrAPI(settings.FLICKR_KEY,
                            settings.FLICKR_SECRET, store_token=False)
    pprint(request.GET)
    frob = request.GET['frob']
    token = f.get_token(frob)
    request.session['token'] = token

    return HttpResponseRedirect(reverse('flickr_auth'))

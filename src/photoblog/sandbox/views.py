
from django.shortcuts import render_to_response

def default_view(request):
    
    return render_to_response('sandbox/default.html')   

from django.conf.urls import patterns, url

urlpatterns = patterns('photoblog.sandbox.views',
    url(r'^$', 'default_view', name='default'),
)


from django.test import TestCase
from django.test.client import Client


class SimpleTest(TestCase):
    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.assertEqual(1 + 1, 2)
        
        
class IntegrationTest(TestCase):
    
    def setUp(self):
        self.Client = Client()
    
    def test_output(self):
        out = self.Client.get('/')
        self.assertEqual(True, out.content.find('<h1>SANDBOX APP</h1>') >= 0)

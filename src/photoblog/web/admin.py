from django.contrib import admin
from django.forms import ModelForm
from django.contrib.admin.widgets import AdminFileWidget
from django.utils.safestring import mark_safe

from sorl.thumbnail.fields import ImageFormField

from photoblog.flickr.models import FlickrPhoto


class FlickrThumbnailWidget(AdminFileWidget):

    def render(self, name, value, attrs=None):

        output = '<img src="{}">'.format(value)

        return mark_safe(u''.join(output))


class FlickPhotoModelForm(ModelForm):

    thumbnail = ImageFormField(required=False, widget=FlickrThumbnailWidget())

    def __init__(self, *args, **kwargs):
        super(FlickPhotoModelForm, self).__init__(*args, **kwargs)
        try:
            instance = kwargs['instance']
            self.fields['thumbnail'].initial = instance.small_320_url
        except(KeyError, AttributeError):
            pass

    def save(self, commit=True):
        self.cleaned_data.pop('thumbnail')

        return super(FlickPhotoModelForm, self).save(commit=commit)

    class Meta:

        model = FlickrPhoto
        exclude = ['exif', 'geo', 'user', 'secret', 'server',]
        labels = {
            "thumbnail": "Thumbnail",
        }


@admin.register(FlickrPhoto)
class FlickrPhotoAdmin(admin.ModelAdmin):

    form = FlickPhotoModelForm

    list_display = ('title', 'flick_thumbnail_square_75', 'publish', 'publish_on_hp',)
    list_editable = ('publish', 'publish_on_hp',)

    readonly_fields = ('flickr_id',)



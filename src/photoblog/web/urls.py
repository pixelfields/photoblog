
from django.conf.urls import *

from .views import *
from photoblog.sitemap.views import *

sitemaps = {
    'index': IndexPageSiteMap,
    'set': SetPageSiteMap,
    'tag': TagPageSiteMap,
    'photo': PhotoPageSiteMap,
}

urlpatterns = patterns('',
    url(r'^$', 'photoblog.web.views.index_view', name='index'),
    url(r'^about/$', 'photoblog.web.views.about_view', name='about'),
    url(r'^tag/(?P<tag>[-\w]+)/$', 'photoblog.web.views.tag_view', name='tag'),
    url(r'^set/(?P<slug>[-\w]+)/$', SetDetailView.as_view(), name='set'),
    url(r'^photo/(?P<slug>[-\w]+)/$', PhotoDetailView.as_view(), name='photo_detail'),
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps})
)
from django import template
import urllib
import cStringIO
from PIL import Image

register = template.Library()


@register.filter
def get_image_width(url):
    try:
        f = cStringIO.StringIO(urllib.urlopen(url).read())
    except Exception:
        return 1920
    try:
        img = Image.open(f)
    except Exception:
        return 1920

    return img[0]

@register.filter
def get_image_height(url):
    try:
        f = cStringIO.StringIO(urllib.urlopen(url).read())
    except Exception:
        return 1080
    try:
        img = Image.open(f)
    except Exception:
        return 1080

    return img[1]

@register.filter
def get_image_size_string(url):
    try:
        f = cStringIO.StringIO(urllib.urlopen(url).read())
    except Exception:
        return "1920x1080"
    try:
        img = Image.open(f)
    except Exception:
        return "1920x1080"

    return "%sx%s" % (img.size[0], img.size[1])






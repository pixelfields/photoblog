# -*- coding: utf-8 -*-
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404
from django.views.generic.detail import DetailView
from django.conf import settings
from django.contrib.auth import get_user_model

from geopy.geocoders import Nominatim
from geopy.exc import GeocoderTimedOut, GeocoderServiceError
from twython import Twython
from taggit.models import Tag

from photoblog.flickr.models import *

user_model = get_user_model()


def index_view(request):

    photos = FlickrPhoto.objects.filter(publish_on_hp=True).filter(publish=True)
    if len(photos) == 0:
        photos = FlickrPhoto.objects.exclude(title__startswith='DSC')[:20]
    sets = FlickrPhotoSet.objects.filter(publish=True)

    context = {
        'photos': photos,
        'sets': sets,
        'current': 'home',
    }

    return render_to_response('web/index.html', context, RequestContext(request))


def about_view(request):

    sets = FlickrPhotoSet.objects.all()

    featured_works = FlickrPhoto.get_latest_by_exif_taken()[:2]

    # twitter
    user = user_model.objects.all()[0]
    twitter = Twython(settings.TWITTER_KEY, settings.TWITTER_SECRET,
                  user.twitterprofile.oauth_token, user.twitterprofile.oauth_secret)
    user_tweets = twitter.get_home_timeline()[:4]

    context = {
        'sets': sets,
        'featured_works': featured_works,
        'tweets': user_tweets,
        'current': 'about',
    }

    return render_to_response('web/about.html', context, RequestContext(request))


def tag_view(request, tag):

    sets = FlickrPhotoSet.objects.all()

    objects = FlickrPhoto.objects.filter(tags__slug__in=[tag])

    tag_name = Tag.objects.filter(slug=tag).distinct()[0]

    context = {
        "sets": sets,
        "objects": objects,
        'current': 'galleries',
        'tag': tag_name,
    }

    return render_to_response('web/tag.html', context, RequestContext(request))


class SetDetailView(DetailView):

    model = FlickrPhotoSet
    template_name = 'web/set_detail.html'

    def get_context_data(self, **kwargs):

        context = super(SetDetailView, self).get_context_data(**kwargs)

        sets = FlickrPhotoSet.objects.all()

        context.update({
            'sets': sets,
            'current': 'galleries',
        })

        return context


class PhotoDetailView(DetailView):

    model = FlickrPhoto
    template_name = 'web/photo_detail.html'

    def get_context_data(self, **kwargs):

        context = super(PhotoDetailView, self).get_context_data(**kwargs)

        address = None
        lat = None
        lng = None
        if self.object.geo is not None:

            geolocator = Nominatim()
            geo_str = '%s, %s' % (self.object.geo.geo_latitude, self.object.geo.geo_longitude)
            lat = self.object.geo.geo_latitude
            lng = self.object.geo.geo_longitude
            try:
                location = geolocator.reverse(geo_str)
                address = location.address
            except GeocoderTimedOut:
                pass
            except GeocoderServiceError:
                pass

        sets = FlickrPhotoSet.objects.all()

        in_sets = FlickrPhotoSet.objects.filter(photos__in=[self.object.id])

        featured_works = FlickrPhoto.get_latest_by_exif_taken()[:6]

        # twitter
        user_tweets = None
        user = user_model.objects.all()[0]
        try:
            twitter = Twython(settings.TWITTER_KEY, settings.TWITTER_SECRET,
                          user.twitterprofile.oauth_token, user.twitterprofile.oauth_secret)
            user_tweets = twitter.get_home_timeline()[:4]
        except Exception:
            pass

        context.update({
            'sets': sets,
            'address': address,
            'in_sets': in_sets,
            'featured_works': featured_works,
            'tweets': user_tweets,
            'current': 'galleries',
            'lat': lat,
            'lng': lng,
        })

        return context
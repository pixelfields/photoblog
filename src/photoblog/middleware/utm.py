# -*- coding: utf-8 -*-
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured

import datetime

class UtmMiddleware:

    def process_response(self, request, response):

        if not hasattr(settings, 'UTM_COOKIE_MAX_AGE'):
            raise ImproperlyConfigured('Settings objects has not attribute UTM_COOKIE_MAX_AGE.')

        max_age = int(getattr(settings, 'UTM_COOKIE_MAX_AGE')) * 24 * 60 * 60
        expires = datetime.datetime.strftime(datetime.datetime.utcnow() + datetime.timedelta(seconds=max_age), "%a, %d-%b-%Y %H:%M:%S GMT")

        if 'utm_source' in request.GET.keys() and request.GET.get('utm_source') != '':
            response.set_cookie('utm_source', request.GET.get('utm_source').encode('utf-8'), max_age=max_age, expires=expires)
        if 'utm_medium' in request.GET.keys() and request.GET.get('utm_medium') != '':
            response.set_cookie('utm_medium', request.GET.get('utm_medium').encode('utf-8'), max_age=max_age, expires=expires)
        if 'utm_term' in request.GET.keys() and request.GET.get('utm_term') != '':
            response.set_cookie('utm_term', request.GET.get('utm_term').encode('utf-8'), max_age=max_age, expires=expires)
        if 'utm_campaign' in request.GET.keys() and request.GET.get('utm_campaign') != '':
            response.set_cookie('utm_campaign', request.GET.get('utm_campaign').encode('utf-8'), max_age=max_age, expires=expires)
        if 'utm_content' in request.GET.keys() and request.GET.get('utm_content') != '':
            response.set_cookie('utm_content', request.GET.get('utm_content').encode('utf-8'), max_age=max_age, expires=expires)

        if 'utm_content' in request.GET.keys() and request.GET.get('utm_content') != '':
            response.set_cookie('utm_content', request.GET.get('utm_content').encode('utf-8'), max_age=max_age, expires=expires)

        ref = request.META.get('HTTP_REFERER', None)
        if ref is not None:
            response.set_cookie('referer', ref.encode('utf-8'), max_age=max_age, expires=expires)

        ip = request.COOKIES.get('ip', None)

        if ip is None:
            if 'HTTP_X_FORWARDED_FOR' in request.META.keys():
                response.set_cookie('ip', request.META.get('HTTP_X_FORWARDED_FOR').encode('utf-8'), max_age=max_age, expires=expires)
            else:
                response.set_cookie('ip', request.META.get('REMOTE_ADDR').encode('utf-8'), max_age=max_age, expires=expires)
        else:
            if 'HTTP_X_FORWARDED_FOR' in request.META.keys() and request.META.get('HTTP_X_FORWARDED_FOR') != request.META.get('REMOTE_ADDR'):
                response.set_cookie('ip', request.META.get('HTTP_X_FORWARDED_FOR').encode('utf-8'), max_age=max_age, expires=expires)


        return response
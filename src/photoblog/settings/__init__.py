#!/usr/bin/python
# -*- coding: UTF-8 -*-

from photoblog.settings.base import *

_MIDDLEWARE_CLASSES = base.MIDDLEWARE_CLASSES
_INSTALLED_APPS = base.INSTALLED_APPS
_TEMPLATE_DIRS = base.TEMPLATE_DIRS

# lines for django cms settings
try:
    from photoblog.settings.cms import *
    
    try:
        _MIDDLEWARE_CLASSES += cms.MIDDLEWARE_CLASSES
    except Exception, e:
        pass
        
    try:
        _INSTALLED_APPS += cms.INSTALLED_APPS
    except Exception, e:
        pass
    
    try:
        _TEMPLATE_DIRS += cms.TEMPLATE_DIRS
    except Exception, e:
        pass
except ImportError, e:
    pass

# lines for local settings
try:
    from photoblog.settings.local import *
    
    try:
        _MIDDLEWARE_CLASSES += local.MIDDLEWARE_CLASSES
    except Exception, e:
        pass
        
    try:
        _INSTALLED_APPS += local.INSTALLED_APPS
    except Exception, e:
        pass
    
    import sys
    if 'test' in sys.argv:
        DATABASES = {
            'default': {
                'ENGINE':'django.db.backends.sqlite3',
                'NAME': ':memory:'
            },
        }
except ImportError, e:
    pass

MIDDLEWARE_CLASSES = _MIDDLEWARE_CLASSES
INSTALLED_APPS = _INSTALLED_APPS
TEMPLATE_DIRS = _TEMPLATE_DIRS

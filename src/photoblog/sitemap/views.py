from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import reverse

from taggit.models import Tag

from photoblog.flickr.models import *


class TagPageSiteMap(Sitemap):

    priority = 0.5
    changefreq = 'monthly'

    def items(self):
        return Tag.objects.all().distinct()

    def location(self, obj):
        return "/%s/%s/" % ('tag', obj.slug)


class SetPageSiteMap(Sitemap):

    priority = 0.5
    changefreq = 'monthly'

    def items(self):
        return FlickrPhotoSet.objects.all()

    def lastmod(self, obj):
        return obj.date_updated

    def location(self, obj):
        return obj.get_absolute_url()


class PhotoPageSiteMap(Sitemap):

    priority = 0.5
    changefreq = 'monthly'

    def items(self):
        return FlickrPhoto.objects.all()

    def lastmod(self, obj):
        if obj.exif.exif_date_taken:
            return obj.exif.exif_date_taken
        else:
            return None

    def location(self, obj):
        try:
            return obj.get_absolute_url()
        except Exception:
            return '/%s/%s/' % ('photo', obj.slug)


class IndexPageSiteMap(Sitemap):

    priority = 0.5
    changefreq = 'weekly'

    def items(self):
        return ['index', 'about',]

    def location(self, obj):
        return reverse(obj)
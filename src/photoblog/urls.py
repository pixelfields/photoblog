from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.http import HttpResponse

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    
    url(r'^sandbox/', include('photoblog.sandbox.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^flickr/', include('photoblog.flickr.urls')),
    url(r'^robots.txt$', lambda r: HttpResponse("User-agent: *\nDisallow: /admin/\nAllow: /", content_type="text/plain")),
    # google webmasters tools verification
    url(r'^googlefa40ece9f2fb5bd5.html$', lambda r: HttpResponse("google-site-verification: googlefa40ece9f2fb5bd5.html", content_type="text/plain")),
    url(r'^', include('photoblog.web.urls')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
